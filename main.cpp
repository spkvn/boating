#include "include/Game.h"

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;
const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 800;

int main() {
    Uint32 frameStart, frameTime;
    bool initSuccess = AGAME::instance()->init("Boating", 100,100,SCREEN_WIDTH,SCREEN_HEIGHT,false);
    if(initSuccess) {
        while(AGAME::instance()->running()){
            frameStart = SDL_GetTicks();
            AGAME::instance()->handleEvents();
            AGAME::instance()->update();
            AGAME::instance()->render();

            frameTime = SDL_GetTicks() - frameStart;
            if(frameTime < DELAY_TIME) {
                SDL_Delay((Uint32) DELAY_TIME - frameTime);
            }
        }
    }
    return 0;
}