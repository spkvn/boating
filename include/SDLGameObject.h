//
// Created by kevin on 17/11/18.
//

#ifndef BOATING_SDLGAMEOBJECT_H
#define BOATING_SDLGAMEOBJECT_H


#include "GameObject.h"
#include "Vector2D.h"

#include "Game.h"
#include "TextureManager.h"

class SDLGameObject : public GameObject {
public:
    SDLGameObject();
    virtual void draw();
    virtual void update();
    virtual void clean();

    void load(const LoaderParams* params);
    Vector2D& getPosition();
    int getWidth();
    int getHeight();
    std::string getTextureId();
    std::string getType();
    void setPosX(float x);
    void setPosY(float y);
protected:
    Vector2D mPosition;
    Vector2D mVelocity;
    Vector2D mAcceleration;

    int mWidth;
    int mHeight;
    int mCurrentRow;
    int mCurrentFrame;
    int mNumFrames;

    std::string mTextureId;
    std::string mType;
};


#endif //BOATING_SDLGAMEOBJECT_H
