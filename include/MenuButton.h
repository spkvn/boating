//
// Created by kevin on 18/11/18.
//

#ifndef BOATING_MENUCURSOR_H
#define BOATING_MENUCURSOR_H

#include <SDL2/SDL_ttf.h>

#include "SDLGameObject.h"


typedef void (*callback_function)(void);

class MenuButton : public SDLGameObject {
private:
    callback_function mCallback;
    bool mHasCursorFocus;

    std::string mDisplayText;
    TTF_Font* mFont;
    SDL_Color mTextColor;
    SDL_Texture* mTextTexture;
    SDL_Surface* mTextSurface;
    SDL_Rect mTextRect;

public:
    MenuButton(std::string displayText, callback_function aCallback,const LoaderParams* parameters);
    void setFocus(bool focus);

    void runCallback();

    void draw();
    void update();

};


#endif //BOATING_MENUCURSOR_H
