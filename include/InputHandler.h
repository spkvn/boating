//
// Created by kevin on 15/11/18.
//

#ifndef BOATING_INPUTHANDLER_H
#define BOATING_INPUTHANDLER_H

#include <SDL2/SDL.h>
#include <vector>
#include <utility>
#include <iostream>

#include "Vector2D.h"
#include "Game.h"


enum mouse_buttons {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2,
};

class InputHandler {
public:
    static InputHandler *instance();

    void update();

    bool getMouseButtonState(int buttonNumber);

    void reset();

    Vector2D *getMousePosition();

    bool isKeyDown(SDL_Scancode key);

private:
    InputHandler();

    ~InputHandler() {}

    static InputHandler *sInstance;

    std::vector<bool> mMouseButtonStates;
    Vector2D *mMousePosition;
    const Uint8 *mKeyStates;

    void onKeyDown();

    void onKeyUp();

    void onMouseMove(SDL_Event &event);

    void onMouseButtonDown(SDL_Event &event);

    void onMouseButtonUp(SDL_Event &event);

};

typedef InputHandler AINPUTHANDLER;

#endif //BOATING_INPUTHANDLER_H
