//
// Created by kevin on 17/11/18.
//

#ifndef BOATING_STATE_H
#define BOATING_STATE_H

#include <vector>
#include <string>

// Abstract base class
class State
{
public:
    virtual void update() = 0;
    virtual void render() = 0;
    virtual bool onEnter() = 0;
    virtual bool onExit() = 0;

    virtual std::string getStateId() const = 0;

protected:
    std::vector<std::string> mTextureIdList;
};

#endif //BOATING_STATE_H
