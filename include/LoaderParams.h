//
// Created by kevin on 17/11/18.
//

#ifndef BOATING_LOADERPARAMS_H
#define BOATING_LOADERPARAMS_H

#include <string>

class LoaderParams {
public:
    LoaderParams(int x, int y, int width, int height, std::string textureID, int numFrames, int callbackID = 0, int animSpeed = 0);

    int getX() const;
    int getY() const;
    int getWidth() const;
    int getHeight() const;
    std::string getTextureID() const;
    int getCallbackID() const;
    int getNumFrames() const;
    int getAnimationSpeed() const;

private:
    int mX;
    int mY;
    int mHeight;
    int mWidth;
    int mNumFrames;
    int mCallbackID;
    int mAnimationSpeed;
    std::string mTextureID;
};


#endif //BOATING_LOADERPARAMS_H
