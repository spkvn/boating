//
// Created by kevin on 2/01/19.
//

#ifndef BOATING_MAST_H
#define BOATING_MAST_H

#include <vector>

#include "SDLGameObject.h"
#include "Sail.h"
#include "SquareSail.h"

class Mast : public SDLGameObject {
private:
    int mHealth;
    Vector2D mBottomOfMast;
    std::vector<Sail*> mSails;
    void recalculateSailPositions(int boatHeading);
public:
    Mast(const LoaderParams* pParams);
    void draw(int boatHeading);
    float totalThrust(float angleToWind);
    void setBottomX(int x);
    void setBottomY(int y);
    void setBottomXY(int x, int y);
};


#endif //BOATING_MAST_H
