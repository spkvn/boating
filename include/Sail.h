//
// Created by kevin on 2/01/19.
//

#ifndef BOATING_SAIL_H
#define BOATING_SAIL_H


#include "SDLGameObject.h"

class Sail : public SDLGameObject {
private:
public:
    virtual float getThrust(float angleFromWind) = 0;
    virtual void draw(int heading) = 0;
};


#endif //BOATING_SAIL_H
