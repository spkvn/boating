//
// Created by kevin on 17/11/18.
//

#ifndef BOATING_GAMEOBJECT_H
#define BOATING_GAMEOBJECT_H

#include "LoaderParams.h"

class GameObject {
public:
    virtual void draw()=0;
    virtual void update()=0;
    virtual void clean()=0;

    virtual void load(const LoaderParams* pParams)=0;
protected:
    GameObject() {}
    virtual ~GameObject() {}
};


#endif //BOATING_GAMEOBJECT_H
