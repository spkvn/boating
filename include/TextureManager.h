//
// Created by kevin on 18/11/18.
//

#ifndef BOATING_TEXTUREMANAGER_H
#define BOATING_TEXTUREMANAGER_H

#include <SDL2/SDL_image.h>
#include <map>

class TextureManager
{
private:
    std::map<std::string, SDL_Texture*> mTextureMap;
    static TextureManager* sInstance;
    TextureManager() {}

public:
    bool load(std::string fileName, std::string id, SDL_Renderer* renderer);
    bool textureExists(const std::string& id);
    void draw(std::string id, int x, int y, int width, int height, SDL_Renderer* renderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void drawFrame(std::string id, int x, int y, int width,  int height, int currentRow, int currentFrame, SDL_Renderer* renderer, SDL_RendererFlip flip = SDL_FLIP_NONE, double angle = 0);
    static TextureManager* instance();
    void clearFromTextureMap(std::string id);
};


//defining singleton.
typedef TextureManager ATEXTUREMANAGER;

#endif //BOATING_TEXTUREMANAGER_H
