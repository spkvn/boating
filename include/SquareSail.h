//
// Created by kevin on 2/01/19.
//

#ifndef BOATING_SQUARESAIL_H
#define BOATING_SQUARESAIL_H


#include "Sail.h"

class SquareSail : public Sail {
private:
    float mReduction;
    float mHealth;
    void calculateFrameRow(int boatHeading);
public:
    SquareSail(LoaderParams* pParams);
    float getThrust(float angleFromWind);
    void draw(int boatHeading);
};


#endif //BOATING_SQUARESAIL_H
