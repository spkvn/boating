// Created by kevin on 15/11/18.

#ifndef BOATING_GAME_H
#define BOATING_GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "InputHandler.h"
#include "StateMachine.h"

class Game {
public:
    ~Game();
    bool init(const char* title, int xPosition, int yPosition, int height, int width, int flag);
    void render();
    void update();
    void handleEvents();
    void clean();
    void quit();

    bool running();

    int getWidth();
    int getHeight();


    SDL_Renderer* getRenderer();
    StateMachine* getStateMachine();

    static Game* instance(){
        if(sInstance == 0) {
            sInstance = new Game();
        }
        return sInstance;
    }
private:
    // Constructor
    Game();

    // initialization methods
    bool setupWindow(const char* title, int xPosition, int yPosition, int height, int width, int flag);

    // Singleton instance pointer;
    static Game* sInstance;

    // Member variables
    SDL_Texture* mpTexture;
    SDL_Window* mpWindow;
    SDL_Renderer* mpRenderer;
    bool mbRunning;
    int miGameWidth;
    int miGameHeight;

    // Game State Machine
    StateMachine* mStateMachine;
};

typedef Game AGAME;

#endif //BOATING_GAME_H
