//
// Created by kevin on 17/11/18.
//

#ifndef BOATING_STATEMACHINE_H
#define BOATING_STATEMACHINE_H

#include "State.h"


class StateMachine {
public:
    void pushState(State* state);
    void changeState(State* state);
    void popState();

    void update();
    void render();
private:
    std::vector<State*> mGameStates;
};


#endif //BOATING_STATEMACHINE_H
