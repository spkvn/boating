//
// Created by kevin on 21/12/18.
//

#ifndef BOATING_BOAT_H
#define BOATING_BOAT_H

#include "SDLGameObject.h"
#include "Mast.h"

class Boat: public SDLGameObject {
private:
    int mHeading;
    Vector2D mDirection;
    int mDirectionRadius;
    void calculateFrameBasedOnHeading();
    float angleFromWind(Vector2D windVector);
    Mast mForeMast;
    Mast mMizzenMast;
public:
    Boat(int heading, const LoaderParams* parameters);

    void draw();
    void update(Vector2D windVector);
};


#endif //BOATING_BOAT_H
