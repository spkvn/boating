//
// Created by kevin on 18/11/18.
//

#ifndef BOATING_MENUSTATE_H
#define BOATING_MENUSTATE_H

#include <vector>

#include "State.h"
#include "MenuButton.h"
#include "PlayState.h"

class MenuState : public State {
public:
    void update();
    void render();
    bool onEnter();
    bool onExit();
    MenuState(bool vertical);
    void setVertical();
    void setHorizontal();
    std::string getStateId() const;
private:
    static std::string sMenuId;
    std::vector<MenuButton> mButtons;
    int mCursorPos;
    bool mVertical;
    void static exampleCallback();
    void static quitGame();
};


#endif //BOATING_MENUSTATE_H
