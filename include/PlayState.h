//
// Created by kevin on 20/12/18.
//

#ifndef BOATING_PLAYSTATE_H
#define BOATING_PLAYSTATE_H


#include "State.h"
#include "Boat.h"

class PlayState : public State {
public:
    void update();
    void render();
    bool onEnter();
    bool onExit();
    std::string getStateId() const;
    PlayState();
private:
    static std::string sPlayId;
    Boat* mBoat;
    Vector2D* mWindVector;
};


#endif //BOATING_PLAYSTATE_H
