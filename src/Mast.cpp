//
// Created by kevin on 2/01/19.
//

#include "../include/Mast.h"

Mast::Mast(const LoaderParams* pParams) : mBottomOfMast(0,0){
    SDLGameObject::load(pParams);
    mHealth = 100;
    mBottomOfMast.setX(mPosition.getX());
    mBottomOfMast.setY(mPosition.getY());

    // Creating Sails
    float sailX = mBottomOfMast.getX() + (mWidth / 4);
    float sail1Y = mBottomOfMast.getY();
    float sail2Y = mBottomOfMast.getY() + mHeight / 4;

    mSails.push_back(
        new SquareSail(new LoaderParams(0, 0, 32, 32, "rough-ass-square-sail", 1))
    );
    mSails.push_back(
        new SquareSail(new LoaderParams(0, 0, 32, 32, "rough-ass-square-sail", 1))
    );
}

void Mast::draw(int boatHeading) {
    ATEXTUREMANAGER::instance()->drawFrame(
        "rough-ass-mast",
        (int) mBottomOfMast.getX() - (mWidth / 2),
        (int) mBottomOfMast.getY() - mHeight,
        mWidth,
        mHeight,
        mCurrentRow,
        mCurrentFrame,
        AGAME::instance()->getRenderer(),
        SDL_FLIP_NONE
    );
    recalculateSailPositions(boatHeading);
    for (int i = 0; i < mSails.size(); i++) {
        mSails[i]->draw(boatHeading);
    }
}

float Mast::totalThrust(float angleToWind) {
    float thrust = 0;
    for (int i = 0; i < mSails.size(); i++) {
        thrust += mSails[i]->getThrust(angleToWind);
    }
    return thrust;
}

void Mast::setBottomX(int x) {
    mBottomOfMast.setX(x);
    mPosition.setX(x - (mWidth / 2));
}

void Mast::setBottomY(int y) {
    mBottomOfMast.setY(y);
    mPosition.setY(y - mHeight);
}

void Mast::setBottomXY(int x, int y) {
    setBottomX(x);
    setBottomY(y);
}

void Mast::recalculateSailPositions(int boatHeading) {
    float sailOneX = mSails[0]->getPosition().getX();
    float sailOneY = mSails[0]->getPosition().getY();
    float sailTwoX = mSails[1]->getPosition().getX();
    float sailTwoY = mSails[1]->getPosition().getY();

    if(boatHeading >= 0 && boatHeading < 30 || boatHeading <= 360 && boatHeading > 330) {
        // up
        sailOneX = mPosition.getX() + (mWidth / 4) + 3;
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + mHeight / 4;
    } else if (boatHeading >= 30 && boatHeading < 90) {
        // up right
        sailOneX = mPosition.getX() + (mWidth / 4);
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + mHeight / 4;
    } else if (boatHeading >= 90 && boatHeading < 120) {
        // right
        sailOneX = mPosition.getX() + (mWidth / 4) + 8;
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + (mHeight / 4);
    } else if (boatHeading >= 120 && boatHeading < 150) {
        // lower right
        sailOneY = mPosition.getY();
        sailTwoY = sailOneY  + (mHeight / 4);
        sailOneX = mPosition.getX() + (mWidth / 4);
        sailTwoX = sailOneX;
    } else if (boatHeading >= 150 && boatHeading < 210) {
        // down
        sailOneX = mPosition.getX() + (mWidth / 4) + 3;
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + mHeight / 4;
    } else if (boatHeading >= 210 && boatHeading < 250 ) {
        // lower left
        sailOneY = mPosition.getY();
        sailTwoY = sailOneY + (mHeight / 4);
        sailOneX = mPosition.getX() + (mWidth / 4);
        sailTwoX = sailOneX;
    } else if (boatHeading >= 250 && boatHeading < 280) {
        // left
        sailOneX = mPosition.getX() + (mWidth / 4) - 8;
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + (mHeight / 4);
    } else if (boatHeading >= 280 && boatHeading < 330 ) {
        // up left
        sailOneX = mPosition.getX() + (mWidth / 4);
        sailTwoX = sailOneX;
        sailOneY = mPosition.getY();
        sailTwoY = mPosition.getY() + mHeight / 4;
    }

    mSails[0]->setPosX(sailOneX);
    mSails[0]->setPosY(sailOneY);
    mSails[1]->setPosX(sailTwoX);
    mSails[1]->setPosY(sailTwoY);
}