//
// Created by kevin on 20/12/18.
//

#include "../include/PlayState.h"

std::string PlayState::sPlayId = "PLAY";

void PlayState::update() {
    // get input, update boat heading
    mBoat->update(*mWindVector);
}

void PlayState::render() {
    // Draw Ocean
    SDL_Rect oceanRect = {0, 0, AGAME::instance()->getWidth(), AGAME::instance()->getHeight()};
    SDL_SetRenderDrawColor(AGAME::instance()->getRenderer(),0x00,0x05,0x56,0xFF); // ocean #000056
    SDL_RenderFillRect(AGAME::instance()->getRenderer(), &oceanRect);

    // draw  boat
    mBoat->draw();
}

bool PlayState::onEnter(){
    // create boat
    std::cout << "Enter PlayState" << std::endl;
    ATEXTUREMANAGER::instance()->load("../assets/rough-ass-boat.png", "rough-ass-boat", AGAME::instance()->getRenderer());
    ATEXTUREMANAGER::instance()->load("../assets/rough-ass-mast.png", "rough-ass-mast", AGAME::instance()->getRenderer());
    ATEXTUREMANAGER::instance()->load("../assets/rough-ass-square-sails.png", "rough-ass-square-sail", AGAME::instance()->getRenderer());
    ATEXTUREMANAGER::instance()->load("../assets/arrow.png", "arrow", AGAME::instance()->getRenderer());
    mBoat = new Boat(0,new LoaderParams(500, 400, 128, 128, "rough-ass-boat", 8));
    mWindVector = new Vector2D(0,0);
}

PlayState::PlayState(){
}

bool PlayState::onExit() {
    delete mBoat;
}

std::string PlayState::getStateId() const {
    return sPlayId;
}