//
// Created by kevin on 18/11/18.
//

#include "../include/MenuButton.h"

MenuButton::MenuButton(std::string displayText, callback_function aCallback, const LoaderParams* parameters) {
    mCallback = aCallback;
    mDisplayText = displayText;
    mHasCursorFocus = false;
    SDLGameObject::load(parameters);

    // set up text.
    mFont = TTF_OpenFont("../assets/fonts/bodonio.ttf", 24);
    mTextColor = {0, 0, 0, 200};
    mTextSurface = TTF_RenderText_Solid(mFont, mDisplayText.c_str(), mTextColor);
    mTextTexture = SDL_CreateTextureFromSurface(AGAME::instance()->getRenderer(),mTextSurface);
    int textWidth = mTextSurface->w;
    int textHeight = mTextSurface->h;
    int width = getWidth();
    int height = getHeight();
    SDL_FreeSurface(mTextSurface);

    // Center Text
    mTextRect.x = (int) mPosition.getX() + ((width - textWidth) / 2);
    mTextRect.w = textWidth;
    mTextRect.y = (int) mPosition.getY() + ((height - textHeight)/ 2);
    mTextRect.h = textHeight;
}


void MenuButton::setFocus(bool focus) {
    mHasCursorFocus = focus;
}


void MenuButton::runCallback() {
    mCallback();
}

void MenuButton::draw() {
    if(mHasCursorFocus){
        mCurrentFrame = 1;
        mCurrentRow = 1;
    } else {
        mCurrentFrame = 0;
        mCurrentRow = 1;
    }
    ATEXTUREMANAGER::instance()->drawFrame(
            "button",
            mPosition.getX(),
            mPosition.getY(),
            mWidth,
            mHeight,
            mCurrentRow,
            mCurrentFrame,
            AGAME::instance()->getRenderer(),
            SDL_FLIP_HORIZONTAL
    );
    SDL_RenderCopy(AGAME::instance()->getRenderer(),mTextTexture, NULL, &mTextRect);
}

void MenuButton::update() {

}