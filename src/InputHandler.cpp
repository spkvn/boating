//
// Created by kevin on 15/11/18.
//

#include "../include/InputHandler.h"

InputHandler* InputHandler::sInstance = nullptr;

InputHandler::InputHandler()
{
    //Handle Mouse buttons (left, mid, right)
    for(int i = 0; i < 3; i++)
    {
        mMouseButtonStates.push_back(false);
    }

    mMousePosition = new Vector2D(0,0);
}

bool InputHandler::isKeyDown(SDL_Scancode key)
{
    if(mKeyStates != nullptr) {
        if(mKeyStates[key] == 1) {
            return true;
        }
    }

    return false;
}

void InputHandler::update()
{
    mKeyStates = SDL_GetKeyboardState(nullptr);
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                AGAME::instance()->quit();
                break;
                
            case SDL_MOUSEMOTION:
                onMouseMove(event);
                break;

            case SDL_MOUSEBUTTONDOWN:
                onMouseButtonDown(event);
                break;

            case SDL_MOUSEBUTTONUP:
                onMouseButtonUp(event);
                break;

            case SDL_KEYDOWN:
                onKeyDown();
                break;

            case SDL_KEYUP:
                onKeyUp();
                break;

            default:
                break;
        }
    }
}


void InputHandler::onKeyDown()
{
    mKeyStates = SDL_GetKeyboardState(nullptr);
}

void InputHandler::onKeyUp()
{
    mKeyStates = SDL_GetKeyboardState(nullptr);
}



void InputHandler::onMouseMove(SDL_Event &event)
{
    mMousePosition->setX(event.motion.x);
    mMousePosition->setY(event.motion.y);
}

void InputHandler::onMouseButtonDown(SDL_Event &event)
{
    if (event.button.button == SDL_BUTTON_LEFT) {
        mMouseButtonStates[LEFT] = true;
    }

    if (event.button.button == SDL_BUTTON_MIDDLE) {
        mMouseButtonStates[MIDDLE] = true;
    }

    if (event.button.button == SDL_BUTTON_RIGHT) {
        mMouseButtonStates[RIGHT] = true;
    }
}

void InputHandler::onMouseButtonUp(SDL_Event &event)
{
    if (event.button.button == SDL_BUTTON_LEFT) {
        mMouseButtonStates[LEFT] = false;
    }

    if (event.button.button == SDL_BUTTON_MIDDLE) {
        mMouseButtonStates[MIDDLE] = false;
    }

    if (event.button.button == SDL_BUTTON_RIGHT) {
        mMouseButtonStates[RIGHT] = false;
    }
}


InputHandler* InputHandler::instance()
{
    if(sInstance == nullptr) {
        sInstance = new InputHandler();
        return sInstance;
    }

    return sInstance;
}

bool InputHandler::getMouseButtonState(int buttonNumber)
{
    return mMouseButtonStates[buttonNumber];
}

Vector2D* InputHandler::getMousePosition()
{
    return mMousePosition;
}


void InputHandler::reset()
{
    mMouseButtonStates[LEFT] = false;
    mMouseButtonStates[RIGHT] = false;
    mMouseButtonStates[MIDDLE] = false;
}