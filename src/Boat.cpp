//
// Created by kevin on 21/12/18.
//

#include "../include/Boat.h"

Boat::Boat(int heading, const LoaderParams *parameters) : mForeMast(parameters), mMizzenMast(parameters), mDirection(0,0) {
    mHeading = heading;
    SDLGameObject::load(parameters);
    mDirectionRadius = mHeight / 2;
    int x = mPosition.getX();
    int y = mPosition.getY();
    mDirection = Vector2D(x, y - mDirectionRadius/2);
    mForeMast = Mast(new LoaderParams(x + 30, y + 32 , 64, 64, "rough-ass-mast",1));
    mMizzenMast = Mast(new LoaderParams(x + 30, y - 16, 64, 64, "rough-ass-mast",1));
    calculateFrameBasedOnHeading();
}

void Boat::calculateFrameBasedOnHeading() {
    float X, Y = 0;
    X = mPosition.getX();
    Y = mPosition.getY();

    if (mHeading >= 0 && mHeading < 30 || mHeading <= 360 && mHeading > 330) {
        // up
        mCurrentRow = 1;
        mCurrentFrame = 1;
        mForeMast.setBottomXY(X + 65,Y + 54);
        mMizzenMast.setBottomXY(X + 65,Y + 81);
    } else if (mHeading >= 30 && mHeading < 90) {
        // up right
        mCurrentRow = 1;
        mCurrentFrame = 2;
        mForeMast.setBottomXY(X + 74,Y + 54);
        mMizzenMast.setBottomXY(X + 42,Y + 81);
    } else if (mHeading >= 90 && mHeading < 120) {
        // right
        mCurrentRow = 2;
        mCurrentFrame = 0;
        mForeMast.setBottomXY(X + 30, Y + 53);
        mMizzenMast.setBottomXY(X + 70, Y + 53);
    } else if (mHeading >= 120 && mHeading < 150) {
        // lower right
        mCurrentRow = 2;
        mCurrentFrame = 1;
        mForeMast.setBottomXY(X + 42, Y + 42);
        mMizzenMast.setBottomXY(X + 72, Y + 72);
    } else if (mHeading >= 150 && mHeading < 210) {
        // down
        mCurrentRow = 2;
        mCurrentFrame = 2;
        mForeMast.setBottomXY(X + 64, Y + 41);
        mMizzenMast.setBottomXY(X + 64, Y + 81);
    } else if (mHeading >= 210 && mHeading < 250 ) {
        // lower left
        mCurrentRow = 3;
        mCurrentFrame = 0;
        mForeMast.setBottomXY(X + 54, Y + 77);
        mMizzenMast.setBottomXY(X + 81, Y + 52);
    } else if (mHeading >= 250 && mHeading < 280) {
        // left
        mCurrentRow = 3;
        mCurrentFrame = 1;
        mForeMast.setBottomXY(X + 48, Y + 53);
        mMizzenMast.setBottomXY(X + 96, Y + 53);
    } else if (mHeading >= 280 && mHeading < 330 ) {
        // up left
        mCurrentRow = 1;
        mCurrentFrame = 0;
        mForeMast.setBottomXY(X + 54, Y + 54);
        mMizzenMast.setBottomXY(X + 84, Y + 84);
    }
}

float Boat::angleFromWind(Vector2D windVector) {
    float degrees = windVector.angle(mPosition);
    float absDifference =  abs(360 - mHeading - degrees);

    if(absDifference > 180) {
        absDifference = 360 - absDifference;
    }
    return absDifference;
}

void Boat::draw(){
    ATEXTUREMANAGER::instance()->drawFrame(
        "rough-ass-boat",
        (int) mPosition.getX(),
        (int) mPosition.getY(),
        mWidth,
        mHeight,
        mCurrentRow,
        mCurrentFrame,
        AGAME::instance()->getRenderer(),
        SDL_FLIP_NONE
    );
    ATEXTUREMANAGER::instance()->drawFrame(
            "arrow",
            (int) mDirection.getX(),
            (int) mDirection.getY(),
            16,
            16,
            1,
            0,
            AGAME::instance()->getRenderer(),
            SDL_FLIP_NONE,
            (double) mHeading
    );

    mForeMast.draw(mHeading);
    mMizzenMast.draw(mHeading);
}

void Boat::update(Vector2D windVector){
    if(AINPUTHANDLER::instance()->isKeyDown(SDL_SCANCODE_LEFT)) {
        if(mHeading > 0) {
            mHeading--;
        } else {
            mHeading = 360;
        }
    } else if (AINPUTHANDLER::instance()->isKeyDown(SDL_SCANCODE_RIGHT)){
        if(mHeading < 360) {
            mHeading++;
        } else {
            mHeading = 0;
        }
    }
    float originX = mPosition.getX() + (mWidth / 2);
    float originY = mPosition.getY() + (mHeight / 2);
    mDirection.setX(originX + (cos(-90 + mHeading * (M_PI / 180))* mDirectionRadius));
    mDirection.setY(originY + (sin(-90 + mHeading * (M_PI / 180))* mDirectionRadius));
    calculateFrameBasedOnHeading();
    float angle = angleFromWind(windVector);

    // calculating thrust.
    float thrust = mForeMast.totalThrust(angle) + mMizzenMast.totalThrust(angle);
    Vector2D diff = mPosition - mDirection;
    mAcceleration.setX(diff.getX()*thrust / 10000);
    mAcceleration.setY(diff.getY()*thrust / 10000);

    SDLGameObject::update();
}
