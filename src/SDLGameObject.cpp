//
// Created by kevin on 17/11/18.
//

#include "../include/SDLGameObject.h"

SDLGameObject::SDLGameObject() : GameObject(),
                                 mPosition(Vector2D(0,0)),
                                 mVelocity(Vector2D(0,0)),
                                 mAcceleration(Vector2D(0,0)),
                                 mWidth(0),
                                 mHeight(0),
                                 mTextureId(""),
                                 mCurrentRow(1),
                                 mCurrentFrame(0),
                                 mNumFrames(0),
                                 mType("")
{
}

void SDLGameObject::load(const LoaderParams *params) {
    mPosition = Vector2D(params->getX(), params->getY());
    mVelocity = Vector2D(0,0);
    mAcceleration = Vector2D(0,0);
    mWidth = params->getWidth();
    mHeight = params->getHeight();
    mTextureId = params->getTextureID();
    mCurrentRow, mCurrentFrame = 0;
    mNumFrames = params->getNumFrames();
}

void SDLGameObject::draw(){
    if(mVelocity.getX() < 0) {
        ATEXTUREMANAGER::instance()->drawFrame(mTextureId, (int) mPosition.getX(), (int) mPosition.getY(), mWidth,
                                               mHeight, mCurrentRow, mCurrentFrame, AGAME::instance()->getRenderer(),
                                               SDL_FLIP_HORIZONTAL);
    } else {
        ATEXTUREMANAGER::instance()->drawFrame(mTextureId, (int) mPosition.getX(), (int) mPosition.getY(), mWidth,
                                               mHeight, mCurrentRow, mCurrentFrame, AGAME::instance()->getRenderer());
    }
}


void SDLGameObject::update() {
    mVelocity += mAcceleration;
    mPosition += mVelocity;
}

void SDLGameObject::clean() {

}

int SDLGameObject::getWidth() {
    return mWidth;
}

int SDLGameObject::getHeight(){
    return mHeight;
}

Vector2D& SDLGameObject::getPosition() {
    return mPosition;
}

std::string SDLGameObject::getTextureId(){
    return mTextureId;
}

std::string SDLGameObject::getType() {
    return mType;
}

void SDLGameObject::setPosX(float x) {
    mPosition.setX(x);
}

void SDLGameObject::setPosY(float y) {
    mPosition.setY(y);
}