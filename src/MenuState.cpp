//
// Created by kevin on 18/11/18.
//

#include "../include/MenuState.h"

std::string MenuState::sMenuId = "MAINMENU";

void MenuState::update() {
    if (AINPUTHANDLER::instance()->isKeyDown(SDL_SCANCODE_UP)){
        if (mCursorPos > 0) {
            mCursorPos--;
        }
    }
    if (AINPUTHANDLER::instance()->isKeyDown(SDL_SCANCODE_DOWN)) {
        if (mCursorPos < mButtons.size() - 1) {
            mCursorPos++;
        }
    }

    for(int i = 0; i < mButtons.size(); i++) {
        if (i == mCursorPos) {
            mButtons[i].setFocus(true);
        } else {
            mButtons[i].setFocus(false);
        }
    }

    if(AINPUTHANDLER::instance()->isKeyDown(SDL_SCANCODE_X)) {
        mButtons[mCursorPos].runCallback();
    }
}

MenuState::MenuState(bool vertical) {
    mVertical = vertical;
}

std::string MenuState::getStateId() const {
    return sMenuId;
}

void MenuState::render() {
    // Draw Background
    // Sky
    SDL_Rect skyRect = {0,0, AGAME::instance()->getWidth(), AGAME::instance()->getHeight()};
    SDL_SetRenderDrawColor(AGAME::instance()->getRenderer(),0x00,0xDC,0xFF,0xFF); // sky blue
    SDL_RenderFillRect(AGAME::instance()->getRenderer(), &skyRect);

    // Horizon
    SDL_Rect horizonRect = {0,AGAME::instance()->getHeight() * 0.66, AGAME::instance()->getWidth(), AGAME::instance()->getHeight()};
    SDL_SetRenderDrawColor(AGAME::instance()->getRenderer(),0xFF,0xFF,0xFF,0xFF); // White
    SDL_RenderFillRect(AGAME::instance()->getRenderer(), &horizonRect);

    // Ocean
    SDL_Rect oceanRect = {0,AGAME::instance()->getHeight() * 0.665, AGAME::instance()->getWidth(), AGAME::instance()->getHeight()};
    SDL_SetRenderDrawColor(AGAME::instance()->getRenderer(),0x00,0x05,0x56,0xFF); // ocean #000056
    SDL_RenderFillRect(AGAME::instance()->getRenderer(), &oceanRect);

    // Draw Buttons
    for(int i = 0; i < mButtons.size(); i++) {
        mButtons[i].draw();
    }
}

bool MenuState::onEnter() {
    ATEXTUREMANAGER::instance()->load("../assets/buttons.png","button", AGAME::instance()->getRenderer());
    setVertical();
    mButtons.push_back(MenuButton("Start", exampleCallback, new LoaderParams(100,100,256,96,"button",2)));
    mButtons.push_back(MenuButton("Quit", quitGame, new LoaderParams(100,216,256,96,"button",2)));
    mCursorPos = 0;
}

bool MenuState::onExit() {

}

void MenuState::setVertical() {
    mVertical = true;
}

void MenuState::setHorizontal() {
    mVertical = false;
}

void MenuState::exampleCallback() {
    AGAME::instance()->getStateMachine()->pushState(new PlayState());
}

void MenuState::quitGame() {
    AGAME::instance()->quit();
}