//
// Created by kevin on 17/11/18.
//

#include "../include/LoaderParams.h"
LoaderParams::LoaderParams(int x, int y, int width, int height, std::string textureID, int numFrames, int callbackID, int animSpeed)
{
    mX = x;
    mY = y;
    mWidth = width;
    mHeight = height;
    mTextureID = textureID;
    mNumFrames = numFrames;
    mCallbackID = callbackID;
    mAnimationSpeed = animSpeed;
}

int LoaderParams::getX() const
{
    return mX;
}

int LoaderParams::getY() const
{
    return mY;
}

int LoaderParams::getWidth() const
{
    return mWidth;
}

int LoaderParams::getHeight() const
{
    return mHeight;
}

std::string LoaderParams::getTextureID() const
{
    return mTextureID;
}

int LoaderParams::getCallbackID() const
{
    return mCallbackID;
}

int LoaderParams::getNumFrames() const
{
    return mNumFrames;
}

int LoaderParams::getAnimationSpeed() const
{
    return mAnimationSpeed;
}