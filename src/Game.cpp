// Created by kevin on 15/11/18.

#include "../include/Game.h"
#include "../include/MenuState.h"

Game *Game::sInstance = 0;


bool Game::init(const char *title, int xPosition, int yPosition, int height, int width, int flag) {
    mbRunning = false;
    if (SDL_Init(SDL_INIT_EVERYTHING) >= 0) {
        TTF_Init();
        mbRunning = setupWindow(title, xPosition, yPosition, height, width, flag);
        if (mbRunning) {
           // start state machine.
            mStateMachine = new StateMachine();
            mStateMachine->changeState(new MenuState(true));
        }
    }
    return mbRunning;
}


bool Game::setupWindow(const char *title, int xPosition, int yPosition, int height, int width, int flag) {
    bool success = false;
    mpWindow = SDL_CreateWindow(title, xPosition, yPosition, width, height, flag);
    miGameHeight = height;
    miGameWidth = width;

    if (mpWindow != 0) {
        mpRenderer = SDL_CreateRenderer(mpWindow, -1, 0);
        SDL_SetRenderDrawColor(mpRenderer, 255, 255, 255, 255);
        success = true;
    }
    return success;
}

void Game::render() {
    SDL_RenderClear(mpRenderer);

    mStateMachine->render();

    SDL_RenderPresent(mpRenderer);
}

void Game::update() {
    // update
    mStateMachine->update();
}

void Game::handleEvents() {
    AINPUTHANDLER::instance()->update();
}

void Game::clean() {
    SDL_DestroyWindow(mpWindow);
    SDL_DestroyRenderer(mpRenderer);
    SDL_Quit();
}

bool Game::running() {
    return mbRunning;
}

void Game::quit() {
    mbRunning = false;
}

SDL_Renderer* Game::getRenderer() {
    return mpRenderer;
}

Game::~Game() {
    delete mpTexture;
    delete mpRenderer;
    delete mpWindow;
}

Game::Game() {
    //
}

int Game::getWidth() {
    return miGameWidth;
}

int Game::getHeight() {
    return miGameHeight;
}

StateMachine* Game::getStateMachine() {
    return mStateMachine;
}