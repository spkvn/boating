//
// Created by kevin on 2/01/19.
//

#include "../include/SquareSail.h"
SquareSail::SquareSail(LoaderParams *pParams){
    mReduction = 0.001;
    SDLGameObject::load(pParams);
}

float SquareSail::getThrust(float angleFromWind){
    float thrust = 0.1 -  mReduction * angleFromWind;
    if (thrust < 0) {
        thrust = 0;
    }
    return thrust;
}

void SquareSail::draw(int boatHeading) {
    calculateFrameRow(boatHeading);
    SDLGameObject::draw();
}


void SquareSail::calculateFrameRow(int boatHeading) {
    if (boatHeading >= 0 && boatHeading < 30 || boatHeading <= 360 && boatHeading > 330) {
        // up
        mCurrentRow = 2;
        mCurrentFrame = 0;
    } else if (boatHeading >= 30 && boatHeading < 90) {
        // up right
        mCurrentRow = 3;
        mCurrentFrame = 0;
    } else if (boatHeading >= 90 && boatHeading < 120) {
        // right
        mCurrentRow = 4;
        mCurrentFrame = 0;
    } else if (boatHeading >= 120 && boatHeading < 150) {
        // lower right
        mCurrentRow = 5;
        mCurrentFrame = 0;
    } else if (boatHeading >= 150 && boatHeading < 210) {
        // down
        mCurrentRow = 6;
        mCurrentFrame = 0;
    } else if (boatHeading >= 210 && boatHeading < 250 ) {
        // lower left
        mCurrentRow = 7;
        mCurrentFrame = 0;
    } else if (boatHeading >= 250 && boatHeading < 280) {
        // left
        mCurrentRow = 8;
        mCurrentFrame = 0;
    } else if (boatHeading >= 280 && boatHeading < 330 ) {
        // up left
        mCurrentRow = 1;
        mCurrentFrame = 0;
    }
}