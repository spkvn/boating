//
// Created by kevin on 18/11/18.
//

#include "../include/TextureManager.h"

TextureManager* TextureManager::sInstance = nullptr;

bool TextureManager::load(std::string fileName, std::string id, SDL_Renderer *renderer) {
    bool success = false;
    SDL_Surface* temporarySurface = IMG_Load(fileName.c_str());
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, temporarySurface);
    SDL_FreeSurface(temporarySurface);
    
    if (texture != 0) {
        mTextureMap[id] = texture;
        success = true;
    }  
    return success;
}

void TextureManager::draw(std::string id, int x, int y, int width, int height, SDL_Renderer *renderer,
                          SDL_RendererFlip flip) {
    SDL_Rect sourceRectangle;
    SDL_Rect destinationRectangle;
    
    sourceRectangle.x = 0;
    sourceRectangle.y = 0;
    sourceRectangle.w = destinationRectangle.w = width;
    sourceRectangle.h = destinationRectangle.h = height;
    destinationRectangle.x = x;
    destinationRectangle.y = y;
    
    SDL_RenderCopyEx(renderer, mTextureMap[id], &sourceRectangle, &destinationRectangle, 0, 0, flip);
}

void TextureManager::drawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame,
                               SDL_Renderer *renderer, SDL_RendererFlip flip, double angle) {
    SDL_Rect sourceRectangle;
    SDL_Rect destinationRectangle;

    sourceRectangle.x = (currentFrame) * width;
    sourceRectangle.y = height * (currentRow - 1);
    sourceRectangle.w = destinationRectangle.w = width;
    sourceRectangle.h = destinationRectangle.h = height;
    destinationRectangle.x = x;
    destinationRectangle.y = y;

    SDL_RenderCopyEx(renderer, mTextureMap[id], &sourceRectangle, &destinationRectangle, angle,0,flip);
}

bool TextureManager::textureExists(const std::string &id) {
    return mTextureMap.count(id) != 0;
}

TextureManager* TextureManager::instance()
{
    if(sInstance == nullptr) {
        sInstance = new TextureManager();
    }

    return sInstance;
}

void TextureManager::clearFromTextureMap(std::string id) {
    mTextureMap.erase(id);
}