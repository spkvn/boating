//
// Created by kevin on 17/11/18.
//

#include "../include/StateMachine.h"

void StateMachine::pushState(State *state) {
    // push and enter the state
    mGameStates.push_back(state);
    mGameStates.back()->onEnter();
}

void StateMachine::changeState(State *state) {
    if (!mGameStates.empty()) {
        if(mGameStates.back()->getStateId() == state->getStateId()) {
           return;
        }
        if(mGameStates.back()->onExit()) {
            delete mGameStates.back();
            mGameStates.pop_back();
        }
    }

    pushState(state);
}

void StateMachine::popState() {
    // if not empty and successfully exits
    if(!(mGameStates.empty()) && mGameStates.back()->onExit()) {
        delete mGameStates.back();
        mGameStates.pop_back();
    }
}

void StateMachine::update() {
    if(!mGameStates.empty()){
        mGameStates.back()->update();
    }
}

void StateMachine::render() {
    if(!mGameStates.empty()){
        mGameStates.back()->render();
    }
}